# arquitetura do seu sistema
O sistema é composto por duas partes, um cliente e um server, no cliente ele solicita o arquivo de um servidor que valida se o arquivo está dentro de sua base ou não, e caso tenha o servidor pode optar por pegar de uma cache, que foi implementada como um arquivo já processado para base64 ou se não está disponivel em cache.

Dado esta verificação é retornado uma resposta caracteristica ao cliente assim como os logs como solicitado

# tecnologias
Foi usado para implementação do servidor o express por facilitar a etapa de desenvolvimento visto que as documentações sobre socket encontrada foi escasas o que impediu que esta vertente fosse mais logica e objetiva.

Para as solicitações do cliente e servidor foi usado o axios por dispor de mais solides para este tipo de aplicação. Assim como  foi usado o fs para a leitura e escrita de arquivo, como o mesmo já lê fazendo a codificação para base64 facilitou bastante o desenvolvimento.

# protocolos

O protocolo usado foi o restFul por ser de simples compreenção e depuração, visto que pode-se usar o postman para testes do servidor e só apos isto dirigir as atenções ao cliente.

# decisões de projetos

As principais decisões de projeto foi relatada nos topicos acima exceto uma, por que usar base64? Esta escolha ocorreu por conta do uso do padrão restful, visto que o arquivo em base64 vira uma grande string, que é suportado pelo json usado no restful.

# soluções para problemas críticos

Inicialmente foi tentado usando socket, mas isto se mostrou inviavel principalmente no espaço de tempo dado, por isto foi decidido que deveria-se adotar outra postura dispensando os pontos extras mas obtendo a pontuação do trabalho.

Outra decisão foi quanto ao uso do express e axios, isto se deu pelo conhecimento previo sobre a api, o que simplifica e agiliza o trabalho alem da vasta documentação destas.
