const express = require('express')
const bodyParser = require('body-parser');
const app = express()
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
const fs = require('fs');
var path = require('path');
const port = 3000
directore = "./temp/"
class index {
    servidor;
    arquivo;
}
class Servidores {
    endereco;
}

app.post('/create', (req, res) => {
    fs.exists(directore + req.body.file, function (exists) {
        if (exists) {
            res.send('Arquivo já existente no servidor')
        } else {
            fs.writeFile(directore + req.body.file, req.body.data, { encoding: 'base64' }, function (err) {
                res.send('Arquivo criado')
            });
        }
    });
})
app.delete('/:filename', (req, res) => {
    fs.exists(directore + req.params.filename, function (exists) {
        if (exists) {
            fs.unlinkSync(directore + req.params.filename, (err) => {
            })
            res.send('Arquivo deletado com sucesso')
        } else {
            res.send('Arquivo não presente no servidor')
        }
    });
})
app.get('/list_file', (req, res) => {
    fs.readdir(directore, (err, files) => {
        if (err) {
            return console.log('Unable to scan directory: ' + err);
        }

        files.forEach(element => {
            console.log(element)
        });
        res.json({
            status: 'success',
            dados: files
        })
    })
})
app.get('/:filename', (req, res) => {
    const contents = fs.readFileSync(directore + req.params.filename, { encoding: 'base64' });
    res.json({
        status: 'success',
        file: directore + req.params.filename,
        dados: contents
    })
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})