module.exports = function (cache) {
    const fs = require('fs');
    var path = require('path');
    directore = "./temp/"
    cachelock = false;
    const controller = {
        create: (req, res) => {
            fs.exists(directore + req.body.file, function (exists) {
                if (exists) {
                    res.send('Arquivo já existente no servidor')
                } else {
                    fs.writeFile(directore + req.body.file, req.body.data, { encoding: 'base64' }, function (err) {
                        res.send('Arquivo criado')
                    });
                }
            });
        },
        delete: (req, res) => {
            fs.exists(directore + req.params.filename, function (exists) {
                if (exists) {
                    fs.unlinkSync(directore + req.params.filename, (err) => {
                    })
                    res.send('Arquivo deletado com sucesso')
                } else {
                    res.send('Arquivo não presente no servidor')
                }
            });
        },
        getList: (req, res) => {
            fs.readdir(directore, (err, files) => {
                if (err) {
                    return console.log('Unable to scan directory: ' + err);
                }

                files.forEach(element => {
                    console.log(element)
                });
                res.json({
                    status: 'success',
                    dados: files
                })
            })
        },
        getFileFronCacheOrSistem: async (req, res) => {
            var cache = controller.loadCache()
            const lcache = await cache.filter((element) => {
                return element.file === req.params.filename;
            })
            if (lcache.length == 1) {
                console.log("Cache hit. " + req.params.filename + " sent to the client")
                res.json({ ...lcache[0] })
                return;
            }
            fs.exists(directore + req.params.filename, function (exists) {
                if (exists) {
                    const contents = fs.readFileSync(directore + req.params.filename, { encoding: 'base64' });
                    const data = {
                        status: 'success',
                        file: req.params.filename,
                        dados: contents
                    }
                    let lencache = JSON.stringify(cache).length
                    cache.push(data)
                    while (JSON.stringify(cache).length >= controller.cachesize) {
                        cache.shift();
                    }
                    lencache = JSON.stringify(cache).length
                    controller.saveCache(cache);
                    console.log("Cache miss. " + req.params.filename + " sent to the client")
                    res.json(data)
                } else {
                    console.log("File " + req.params.filename + " does not exist")
                    res.json({
                        status: 'err',
                        menssage: "Arquivo não existente no servidor"
                    })
                }
            });
        },
        getFile: async (req, res) => {
            var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            console.log("Client " + ip + " is requesting file " + req.params.filename)
            const func2 = () => {
                if (cachelock == true) {
                    setTimeout(func2, 3000);
                    return;
                } else {
                    controller.getFileFronCacheOrSistem(req,res);
                }
            }
            setTimeout(func2, 3000);


        },
        cachefile: './cache.json',
        cachesize: 64 * 1024,
        saveCache(cache) {
            // let data = JSON.stringify(cache);
            console.log("Cachedestravada")
            // fs.writeFileSync(controller.cachefile, data);
            cachelock = false;
        },
        loadCache() {
            cachelock = true;
            console.log("CacheTravada")
            // let cache = fs.readFileSync(controller.cachefile);
            // cache = JSON.parse(cache);
            return cache;

        }
    }
    return controller;

}