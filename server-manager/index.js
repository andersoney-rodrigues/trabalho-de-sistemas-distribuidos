const express = require('express')
const bodyParser = require('body-parser');
const app = express()
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

let cache=[]
const router = require('./router')(cache)
const port = 3000
class index {
    servidor;
    arquivo;
}
class Servidores {
    endereco;
}

app.use('', router);

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})