
module.exports = function (cache) {
    var express = require('express');
    var router = express.Router();

    const fileController = require('../contollers/file.controller')(cache)
    // middleware that is specific to this router
    router.post('/create', fileController.create)
    router.delete('/:filename', fileController.delete)
    router.get('/list_file', fileController.getList)
    router.get('/:filename', fileController.getFile)

    return router;
};