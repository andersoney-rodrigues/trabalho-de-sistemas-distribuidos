module.exports = function (cache) {

    var express = require('express');
    var router = express.Router();
    const fileRouter = require('./router/file.router')(cache)
    // middleware that is specific to this router
    router.use('/file', fileRouter)

    return router;

}